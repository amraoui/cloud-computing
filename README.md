Jupyter Notebooks of the course Cloud computing at EURECOM
Teacher Pietro Michiardi

**Lab on k-means :**

In this Notebook, we'll focus on the development of a simple distributed algorithm. As for the Notebook on SGD, we focus on iterative algorithms, which eventually converge to a desired solution.
In what follows, we'll proceed with the following steps:
- We first introduce formally the  k-means algorithm
- Then we focus on a serial implementation. To do this, we'll first generate some data using scikit. In passing, we'll also use the -means implementation in scikit to have a baseline to compare against.
- Subsequently, we will focus on some important considerations and improvements to the serial implementation of  k
k-means.
- At this point, we'll design our distributed version of the  k
k-means algorithm using pyspark, and re-implement the enhancements we designed for the serial version

**Lab on Stochastic Gradient Descent (SGD) :**

The goal of this notebook is to work on distributed optimization algorithms, which are the foundation for large scale analytics and machine learning. Specifically, we will focus on the details of stochastic gradient descent (SGD). To do so, we will work on a simple regression problem, where we will apply SGD to minimize a loss function, as defined for the problem at hand. 
Next, an outline of the steps we will follow in this Notebook:
- Brief introduction to linear regression
- Implementation of serial algorithms: from Gradient Descent, to Stochastic Gradient Descent
- Implementation of distributed algorithms with Apache Spark

**Lab on Spark SQL :**

In this notebook, we will learn how to use the DataFrame API and SparkSQL to perform simple data analytics tasks.
The main goals of this notebook are the following:
- Understand the advantages and disadvantages of using DataFrame over RDD
- Analyze the airline data with the DataFrame API and SparkSQL